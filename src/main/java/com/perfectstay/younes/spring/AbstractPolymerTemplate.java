package com.perfectstay.younes.spring;

import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.templatemodel.TemplateModel;

public abstract class AbstractPolymerTemplate<T extends TemplateModel> extends PolymerTemplate<T> {
    abstract public void load(Long bookingId);

}

