package com.perfectstay.younes.spring;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.templatemodel.TemplateModel;

@Tag("booking-view")
@HtmlImport("src/booking-view.html")
public class BookingView extends PolymerTemplate<TemplateModel> {

    @Id("headerComponentView")
    private GridView headerComponentView;

    public BookingView() {
        headerComponentView.load();
    }

 }
