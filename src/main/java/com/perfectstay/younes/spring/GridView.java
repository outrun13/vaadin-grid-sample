package com.perfectstay.younes.spring;

import com.perfectstay.younes.spring.dto.EstimateItemDto;
import com.perfectstay.younes.spring.grid.EstimateItemGrid;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.templatemodel.TemplateModel;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Tag("header-view")
@HtmlImport("src/grid-view.html")
@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GridView extends PolymerTemplate<TemplateModel> {

    @Id("divGrid")
    private Div divGrid;

    public GridView() {}

    public void load() {
        divGrid.removeAll();
        EstimateItemGrid estimateItemGrid = new EstimateItemGrid();
        List<EstimateItemDto> estimateItemDtos = getEstimateItemDtos();

        Stream<EstimateItemDto> estimateItemDtoStream = new ArrayList<EstimateItemDto>().stream();
        estimateItemGrid.setColumns(estimateItemDtoStream);

        ListDataProvider<EstimateItemDto> dataProvider = new ListDataProvider<>(estimateItemDtos);
        estimateItemGrid.setDataProvider(dataProvider);
        divGrid.add(estimateItemGrid);
    }

    private List<EstimateItemDto> getEstimateItemDtos() {
        List<EstimateItemDto> estimateItemDtos = new ArrayList<>();
        EstimateItemDto estimateItemDto1 = new EstimateItemDto();
        estimateItemDto1.setName("NAME");
        estimateItemDto1.setQuantity(58);
        estimateItemDto1.setTotalCost(new BigDecimal(5233));

        EstimateItemDto estimateItemDto2 = new EstimateItemDto();
        estimateItemDto2.setName("NAME2");
        estimateItemDto2.setQuantity(578);
        estimateItemDto2.setTotalCost(new BigDecimal(3235));

        EstimateItemDto estimateItemDto3 = new EstimateItemDto();
        estimateItemDto3.setName("NAME3");
        estimateItemDto3.setQuantity(53);
        estimateItemDto1.setTotalCost(new BigDecimal(322));

        estimateItemDtos.add(estimateItemDto1);
        estimateItemDtos.add(estimateItemDto2);
        estimateItemDtos.add(estimateItemDto3);
        return estimateItemDtos;
    }
}
