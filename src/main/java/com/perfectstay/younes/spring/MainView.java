package com.perfectstay.younes.spring;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.page.History;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The main view contains a simple label element and a template element.
 */
@HtmlImport("styles/shared-styles.html")
@Route
@Theme(Lumo.class)
public class MainView extends VerticalLayout {

    public MainView() {
        // This is just a simple label created via Elements API
        Button button = new Button("Click me To show Grid",
                event -> {
                        UI.getCurrent().navigate("route-example");
                });
        // This is a simple template example


        Tab tab1 = new Tab("Tab one");
        ExampleTemplate exampleTemplate1 = new ExampleTemplate();
        exampleTemplate1.load(2L);

        Tab tab2 = new Tab("Tab two");
        ExampleTemplate exampleTemplate2 = new ExampleTemplate();
        exampleTemplate2.setVisible(false);

        Map<Tab, AbstractPolymerTemplate> tabsToPages = new HashMap<>();
        tabsToPages.put(tab1, exampleTemplate1);
        tabsToPages.put(tab2, exampleTemplate2);
        Tabs tabs = new Tabs(tab1, tab2);
        Div pages = new Div(exampleTemplate1, exampleTemplate2);
        Set<Component> pagesShown = Stream.of(exampleTemplate1)
                .collect(Collectors.toSet());

        tabs.addSelectedChangeListener(event -> {
            pagesShown.forEach(page -> page.setVisible(false));
            pagesShown.clear();
            AbstractPolymerTemplate selectedPage = tabsToPages.get(tabs.getSelectedTab());

            selectedPage.setVisible(true);
            selectedPage.load(1L);

            pagesShown.add(selectedPage);
        });

        add(tabs);
        add(pages);
        setClassName("main-layout");
    }
}
