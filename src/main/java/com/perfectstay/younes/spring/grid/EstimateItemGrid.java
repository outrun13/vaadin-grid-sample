package com.perfectstay.younes.spring.grid;

import com.perfectstay.younes.spring.dto.EstimateItemDto;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.function.ValueProvider;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EstimateItemGrid  extends Grid<EstimateItemDto> {

    public void setColumns(Stream<EstimateItemDto> estimateItemDtoStream) {
        ListDataProvider<EstimateItemDto> dataProvider = new ListDataProvider<>(estimateItemDtoStream.collect(Collectors.toList()));
        List<ValueProvider<EstimateItemDto, String>> valueProviders = new ArrayList<>();

        valueProviders.add(EstimateItemDto::getName);
        valueProviders.add(estimateItemDto -> estimateItemDto.getTotalPrice()!=null ? String.valueOf(estimateItemDto.getTotalPrice()) + " " + estimateItemDto.getCurrency()  : "na");
        valueProviders.add(estimateItemDto -> estimateItemDto.getTotalCost()!=null ? String.valueOf(estimateItemDto.getTotalCost())  + " " +  estimateItemDto.getCurrency()  : "na");
        valueProviders.add(estimateItemDto -> estimateItemDto.getMargin()!=null ? String.valueOf(estimateItemDto.getMargin())  + " " +  estimateItemDto.getCurrency() : "na");
        valueProviders.add(EstimateItemDto::getCurrency);

        Iterator<ValueProvider<EstimateItemDto, String>> iterator = valueProviders.iterator();
        this.setDataProvider(dataProvider);

        this.addColumn(iterator.next()).setHeader("name").setWidth("300px");
        this.addColumn(iterator.next()).setHeader("price").setWidth("100px");
        this.addColumn(iterator.next()).setHeader("cost").setWidth("100px");
        this.addColumn(iterator.next()).setHeader("margin").setWidth("100px");
    }
}